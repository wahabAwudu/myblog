from django import forms

from .models import Post


class PostForm(forms.ModelForm):
    published = forms.DateTimeField(widget=forms.SelectDateWidget())
    class Meta:
        fields = ['image', 'title', 'text', 'draft', 'published']
        model = Post
