from rest_framework.permissions import BasePermission

class IsOwnerOrReadOnly(BasePermission):
    message = "You are not the Owner of this Post"
    def has_object_permission(self, request, obj, view):
        return obj.author == request.user.id
