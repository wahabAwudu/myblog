from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination

# this make you tell the limit of items on a page and the pages will auto-generate.
class PostListLimitOffsetPagination(LimitOffsetPagination):
    max_limit = 10
    default_limit = 2

# this actually make you explicitly define the number of items you wanner have on each page.
class PostListPageNumberPagination(PageNumberPagination):
    page_size = 10