from rest_framework.serializers import ModelSerializer, HyperlinkedIdentityField, SerializerMethodField

from myblog.apps.blog.models import Post

from myblog.apps.comment.api.serializers import CommentListAPISerializer
from myblog.apps.comment.models import Comment


class PostCreateSerializer(ModelSerializer):
    user = SerializerMethodField()

    class Meta:
        model = Post
        fields = ['user', 'title', 'text', 'published']

        def get_user(self, obj):
            return str(obj.author)


class PostListSerializer(ModelSerializer):
    post = HyperlinkedIdentityField(
        view_name='post-api:detail',
        lookup_field='slug'
    )
    user = SerializerMethodField()

    class Meta:
        model = Post
        fields = ['post', 'user', 'title', 'published']

    def get_user(self, obj):
        return str(obj.author)


class PostDetailSerializer(ModelSerializer):
    user = SerializerMethodField()
    image = SerializerMethodField()
    comments = SerializerMethodField()
    update = HyperlinkedIdentityField(
        view_name='post-api:update',
        lookup_field='slug'
    )
    delete = HyperlinkedIdentityField(
        view_name='post-api:delete',
        lookup_field='slug'
    )

    class Meta:
        model = Post
        fields = ['user', 'title', 'text', 'slug', 'image', 'comments', 'update', 'delete']

    def get_user(self, obj):
        return str(obj.author)

            # returns the url of the post image
    def get_image(self, obj):
        try:
            image = obj.image.url
        except:
            image = None
        return image

        # returns all comments related to the current post
    def get_comments(self, obj):
        this_object_comments = Comment.objects.comment_by_instance(obj)
        return CommentListAPISerializer(this_object_comments, many=True).data


class PostUpdateSerializer(ModelSerializer):
    author = SerializerMethodField()

    class Meta:
        model = Post
        fields = ['author', 'title', 'text']

    def get_author(self, obj):
        return str(obj.author)


class PostDeleteSerializer(ModelSerializer):
    author = SerializerMethodField()

    class Meta:
        model = Post
        fields = ['author', 'title']

    def get_author(self, obj):
        return str(obj.author)

