from django.apps import AppConfig


class BlogConfig(AppConfig):
    name = 'config.blog'
