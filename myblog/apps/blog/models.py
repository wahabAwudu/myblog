import datetime
import time
import math

from django.db import models
from django.core.urlresolvers import reverse
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import slugify
from django.contrib.contenttypes.fields import ContentType

from myblog.apps.comment.models import Comment


def upload_location(instance, filename):
    return "blog/%s" % (filename)

@python_2_unicode_compatible
class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=140, verbose_name='Post Title')
    text = models.TextField(verbose_name='Body of Post')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Posted Date')
    updated = models.DateTimeField(auto_now=True, verbose_name='Updated Date')

    # for the image field. You need to install pillow for it to work
    width_field = models.IntegerField(default=0)
    height_field = models.IntegerField(default=0)
    image = models.ImageField(upload_to=upload_location, width_field='width_field', height_field='height_field', verbose_name='Your Image (Not More than 200kb)')

    # for us to publish at a time we wish even in the future.
    draft = models.BooleanField(default=False, verbose_name='Render Inactive')
    published = models.DateTimeField(auto_now=False, auto_now_add=False, verbose_name='Date to be Published')

    slug = models.SlugField(unique=True, max_length=140)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:detail', kwargs={'slug': self.slug})

    class Meta:
        ordering = ['-created', '-updated']

    def get_unique_slug(self):
        slug = slugify(self.title)
        unique_slug = slug
        num = 1
        while Post.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num +=1
        return unique_slug

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self.get_unique_slug()
        super().save()

    # returns all comments for this particular 'self' post
    @property
    def comment(self):
        return Comment.objects.comment_by_instance(self)

    @property
    def get_content_type(self):
        return ContentType.objects.get_for_model(self.__class__)
