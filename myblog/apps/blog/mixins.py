from django.core.exceptions import PermissionDenied
from django.contrib.auth.views import redirect_to_login
from django.core.urlresolvers import reverse_lazy
from django.utils import timezone
from django.conf import settings
from .models import Post

class QuerysetModelMixin(object):
    queryset = Post.objects.all().filter(published__lte=timezone.now(), draft=False)


class AuthNotStaffMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied
        return super(AuthNotStaffMixin, self).dispatch(request, *args, **kwargs)


class AuthNotUserMixin(object):
    login_url = settings.LOGIN_URL

    def dispatch(self, request, *args, **kwargs):
        if not request.user is self.get_object().user:
            return redirect_to_login(request.get_full_path(), self.login_url)
        return super(AuthNotUserMixin, self).dispatch(request, *args, **kwargs)
