from django.contrib import admin
from .models import Post
from .forms import PostForm

class PostAdmin(admin.ModelAdmin):
    class Meta:
        list_display = ['__str__', 'created', 'updated', 'published', 'author']
        form = PostForm

admin.site.register(Post, PostAdmin)

