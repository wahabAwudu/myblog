from django.conf.urls import url
from .views import CreatePostView, ListPostView, post_detail_view, PostUpdateView, DeletePostView

urlpatterns = [
    url(r'^$', ListPostView.as_view(), name='home'),
    url(r'^create/$', CreatePostView.as_view(), name='create'),
    url(r'^(?P<slug>[-\w]+)/$', post_detail_view, name='detail'),
    url(r'^(?P<slug>[-\w]+)/update/$', PostUpdateView.as_view(), name='update'),
    url(r'^(?P<slug>[-\w]+)/delete/$', DeletePostView.as_view(), name='delete'),
]
