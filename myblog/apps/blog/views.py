from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404, redirect, render_to_response
from django.core.urlresolvers import reverse_lazy
from django.utils import timezone
from django.views.generic import CreateView, DetailView, UpdateView, DeleteView, ListView, FormView
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType

from .models import Post
from .forms import PostForm
from myblog.apps.comment.models import Comment
from myblog.apps.comment.forms import CommentForm

from .mixins import QuerysetModelMixin
from braces import views

#create a post
class CreatePostView(CreateView):
    model = Post
    form_class = PostForm
    success_url = reverse_lazy('blog:home')

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.draft = False
        form.instance.published = timezone.now()
        return super(CreatePostView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "CREATE A NEW POST"
        return context



#list all the posts
class ListPostView(ListView):
    model = Post
    paginate_by = 3

    def get_queryset(self):
        return Post.objects.filter(published__lte=timezone.now(), draft=False)

    def get_context_data(self, **kwargs):
        context = super(ListPostView, self).get_context_data(**kwargs)
        context['title'] = "Hello My Blog Home Page"
        return context


def post_detail_view(request, slug=None):
    instance = get_object_or_404(Post, slug=slug)

    form = CommentForm(request.POST or None)
    parent_id = None
    parent_of_reply = None

    try:
        parent_id = int(request.POST.get("parent_id"))
    except:
        parent_id = None
        
    if parent_id:
        parent = Comment.objects.filter(id=parent_id)
        if parent.exists():
            parent_of_reply = parent.first()

    if form.is_valid():
        Comment.objects.get_or_create(
            user = request.user,
            content_type = instance.get_content_type,
            object_id = instance.id,
            comment_text = form.cleaned_data.get("content"),
            parent = parent_of_reply,
            )
        form = CommentForm()

    context = {
        "comments": instance.comment,
        "instance": instance,
        "form": form
    }
    return render(request, "blog/post_detail.html", context)
        

#update the post
class PostUpdateView(UpdateView):
    model = Post
    form_class = PostForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Update POST NOW!!"
        return context
    
    # permission_required = "auth.change_user"
    # form_class = PostForm


#delete post
class DeletePostView(DeleteView):
    model = Post
    success_url = '/'
