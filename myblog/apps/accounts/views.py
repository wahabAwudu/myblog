from django.contrib.auth import login, logout, authenticate, get_user_model
from django.shortcuts import render, redirect

from .forms import UserLoginForm, UserRegisterForm

def register_view(request):
    title = "Register Free"
    button_text = "Join Us"
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        form_instance = form.save(commit=False)
        password = form_instance.password
        username = form_instance.username
        # unique way of saving the password
        form_instance.set_password(password)
        form_instance.save()
        authenticate_user = authenticate(username=username, password=password)
        login(request, authenticate_user)
        return redirect("home")

    return render(request, "accounts/auth_form.html", {'form':form, 'title':title, 'button_text':button_text})


def login_view(request):
    form = UserLoginForm(request.POST or None)
    title = "Login Form"
    button_text = "Login"

    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        authenticate_user = authenticate(username=username, password=password)
        login(request, authenticate_user)
        return redirect('home')
    return render(request, "accounts/auth_form.html", {'form':form, "title":title, "button_text":button_text})


def logout_view(request):
    logout(request)
    return redirect("home")
    return render(request, "accounts/logout.html", {})




