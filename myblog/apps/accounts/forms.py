from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User


class UserRegisterForm(forms.ModelForm):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput)
    email = forms.EmailField()
    email2 = forms.EmailField(label="Confirm Email")
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'email2',
            'password'
        ]

    def clean(self, *args, **kwargs):
        email = self.cleaned_data.get("email")
        email2 = self.cleaned_data.get("email2")

        if email != email2:
            raise forms.ValidationError("Emails Must Match")
        
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError("Email Already Registered")

        return super(UserRegisterForm, self).clean(*args, **kwargs)


class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
    
    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")

        authenticate_user = authenticate(username=username, password=password)
        
        if not authenticate_user:
            raise forms.ValidationError("This user does not Exist")

        if not authenticate_user.check_password(password):
            raise forms.ValidationError("Password is Incorrect")

        if not authenticate_user.is_active:
            raise forms.ValidationError("Account is Inactive")

        return super(UserLoginForm, self).clean(*args, **kwargs)
