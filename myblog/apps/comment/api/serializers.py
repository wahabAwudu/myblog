from rest_framework.serializers import (
ModelSerializer,
HyperlinkedIdentityField,
SerializerMethodField
)

from myblog.apps.comment.models import Comment


class CommentListAPISerializer(ModelSerializer):
    reply_count = SerializerMethodField()

    class Meta:
        model = Comment
        fields = [
            'id',
            'content_type',
            'object_id',
            'comment_text',
            'parent',
            'commented_at',
            'reply_count'
        ]
    
    def get_reply_count(self, obj):
        if obj.is_parent:
            return obj.replies().count()
        return 0


class CommentReplyAPISerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = [
            'id',
            'comment_text',
            'commented_at',
        ]


class CommentDetailAPISerializer(ModelSerializer):
    user = SerializerMethodField()
    replies = SerializerMethodField()
    reply_count = SerializerMethodField()

    class Meta:
        model = Comment
        fields = [
            'user',
            'content_type',
            'object_id',
            'comment_text',
            'commented_at',
            'replies',
            'reply_count'
        ]

    def get_user(self, obj):
        return str(obj.user)

    def get_replies(self, obj):
        if obj.is_parent:
            return CommentReplyAPISerializer(obj.replies(), many=True).data
        return None

    def get_reply_count(self, obj):
        if obj.is_parent:
            return obj.replies().count
        return 0


class CommentDeleteAPISerializer(ModelSerializer):
    user = SerializerMethodField()

    class Meta:
        model = Comment
        fields = [
            'user',
            'content_type',
            'object_id',
            'comment_text'
            ]

    def get_user(self, obj):
        return obj.user
