from rest_framework.generics import RetrieveAPIView, ListAPIView, DestroyAPIView

from myblog.apps.comment.models import Comment
from .serializers import CommentListAPISerializer, CommentDetailAPISerializer


class CommentListAPIView(ListAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentListAPISerializer


class CommentDetailAPIView(RetrieveAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentDetailAPISerializer
