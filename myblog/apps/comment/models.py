from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class CommentManager(models.Manager):
    def comment_by_instance(self, instance):
        cont_type = ContentType.objects.get_for_model(instance.__class__)
        return super(CommentManager, self).filter(content_type=cont_type, object_id=instance.id, parent=None)


@python_2_unicode_compatible
class Comment(models.Model):
    user = models.ForeignKey('auth.User')
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    parent = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True)
    comment_text = models.TextField()
    commented_at = models.DateTimeField(auto_now_add=True)

    objects = CommentManager()

    class Meta:
        ordering = ['-commented_at']

    def __str__(self):
        return str(self.user.username)

    #used to get replies as children of the comments
    def replies(self):
        return Comment.objects.filter(parent=self)

    @property
    def is_parent(self):
        if self.parent is None:
            return True
        return False
