from django.apps import AppConfig


class CommentConfig(AppConfig):
    name = 'config.comment'
