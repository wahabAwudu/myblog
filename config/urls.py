from django.conf.urls import include,url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import TemplateView


urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html'), name="home"),
    url(r'^blog/', include('myblog.apps.blog.urls', namespace="blog")),
    url(r'^custom-auth/', include('myblog.apps.accounts.urls')),
    url(r'^accounts/', include('allauth.urls')),

    url(r'^api/comment/', include('myblog.apps.comment.api.urls', namespace='comment-api')),
    url(r'^api/post/', include('myblog.apps.blog.api.urls', namespace='post-api')),
    url(r'^admin/', admin.site.urls),
]

# if settings.DEBUG:
#     urlpatterns += static(settings.STATIC_URL, document_root= settings.STATIC_ROOT)
#     urlpatterns += static(settings.MEDIA_URL, document_root= settings.MEDIA_ROOT)
