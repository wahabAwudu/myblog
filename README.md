# myblog
this is a Blog built with Django v1.11 and Bootstrap3.
* how to setup *
* create a postgresql database by running 'create database myblogdb;'
* You can create a new superuser by 'python manage.py createsuperuser' and follow the prompt
* create a virtual environment and activate it
* install the requirements of the project by running 'pip install -r requirements/local.txt'
* now, collect all the project's static files into the staticfiles directory by 'python manage.py collectstatic'
* start the development server by 'python manage.py runserver'


* the code base was written with the django Class Based View and Function Based View paradigms together with mixin implementations with from django-braces.

* Cody Regards
